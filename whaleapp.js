var whaleApp = angular.module('whaleApp', ['ngRoute']);

whaleApp.config(function ($routeProvider, $locationProvider) {
    $routeProvider
        .when('/login', {
            templateUrl: 'tpl/login.html',
            controller: 'LoginCtrl'
        })
        .when('/signup', {
            templateUrl: 'tpl/signup.html',
            controller: 'SignupCtrl'
        })
        .when('/search', {
            templateUrl: 'tpl/search.html',
            controller: 'SearchCtrl',
            secure: true
        }).
        otherwise({
            redirectTo: '/search'
        });
});

whaleApp.filter('searchFilter', function () {
    return function (items, filterData) {
        return typeof items === 'undefined' || items.filter(function (item) {
            for (var filter in filterData) {
                var fdata = filterData[filter];
                if (filterData.hasOwnProperty(filter) && typeof fdata !== 'undefined' && fdata !== '') {
                    switch (filter) {
                        case 'dateFrom':
                            if (item.issueDate < Date.parse(fdata)) {
                                return false;
                            }
                            break;
                        case 'dateTo':
                            if (item.issueDate > Date.parse(fdata)) {
                                return false;
                            }
                            break;
                        case 'priceFrom':
                            if (item.price < parseFloat(fdata)) {
                                return false;
                            }
                            break;
                        case 'priceTo':
                            if (item.price > parseFloat(fdata)) {
                                return false;
                            }
                            break;
                        case 'color':
                            if (item.color.toLowerCase() !== fdata) {
                                return false;
                            }
                            break;
                        case 'inStock':
                            if (fdata === true && !item.inStock) {
                                return false;
                            }
                            break;
                    }
                }
            }
            return true;
        });
    }
});

whaleApp.directive("itemRating", function () {
    return function (scope, element, attrs) {
        var rating = parseInt(attrs.itemRating);
        if (!isNaN(rating)) {
            var footer = $('<div class="panel-footer"></div>');
            for (var i = 0; i < rating; i++) {
                footer.append('<span style="color: gold;" class="glyphicon glyphicon-star"></span>');
            }
            element.append(footer);
        }
    }
});

whaleApp.run(function ($rootScope, $location, Auth) {
    $rootScope.$on('$routeChangeStart', function (event, next, current) {
        if (next && next.$$route && next.$$route.secure) {
            if (!Auth.isAuthorized()) {
                $rootScope.$evalAsync(function () {
                    $location.path('/login');
                });
            }
        }
    });
});

whaleApp.service('Cart', function () {

    var cart = {};
    var cartItems = [];
    var cartLength = 0;

    function refreshItems() {
        cartItems = [];
        for (var key in cart) {
            if (cart.hasOwnProperty(key)) {
                cartItems.push({
                    id: key,
                    name: cart[key].item.name,
                    count: cart[key].count
                })
            }
        }
    }

    this.getItems = function () {
        return cartItems;
    };

    this.getCount = function () {
        return cartLength;
    };

    this.addItem = function (item) {
        if (!cart[item.id]) {
            cart[item.id] = {
                item: item,
                count: 0
            }
        }
        cart[item.id].count += 1;
        cartLength++;
        refreshItems();
    }

    this.removeItem = function (id) {
        if (cart[id]) {
            if (cart[id].count === 1) {
                delete cart[id];
            } else {
                cart[id].count -= 1;
            }
            cartLength--;
            refreshItems();
        }
    }

});

whaleApp.factory('Auth', function () {

    var user = null;
    var isAuthorized = false;

    return {
        login: function (email, password) {
            var pass = localStorage.getItem(email);
            if (!pass || pass !== toMD5(password)) {
                return false;
            }
            isAuthorized = true;
            user = email;
            return true;
        },
        isAuthorized: function () {
            return isAuthorized;
        }
    };

});

whaleApp.factory('Item', function ($q, $http) {

    return {
        list: function () {
            return $q(function (resolve, reject) {
                $http.get('items.json').then(function (response) {
                    if (response.data.length) {
                        resolve(response.data);
                    }
                }, function () {
                    reject('Error');
                });
            })
        }
    };

});

whaleApp.controller('HeaderCtrl', function ($rootScope, $scope, $location, Auth, Cart) {
    $scope.cart = Cart;
    $scope.isAuthorized = Auth.isAuthorized();

    $scope.getLinkClass = function(path) {
        if ($location.path().substr(0, path.length) == path) {
            return "active"
        } else {
            return ""
        }
    };

    $scope.removeItem = function (e, id) {
        e.preventDefault();
        Cart.removeItem(id);
    };

    $scope.$watch(Auth.isAuthorized, function (value, oldValue) {

        $rootScope.$applyAsync(function () {
            $scope.isAuthorized = value;
        })

    }, true);

});

whaleApp.controller('LoginCtrl', function ($rootScope, $scope, $location, Auth) {
    $scope.email = '';
    $scope.password = '';

    $scope.error = '';

    $scope.login = function () {
        $scope.error = '';
        if ($scope.email === '') {
            $scope.error = 'Email cannot be empty';
            return;
        }
        if ($scope.password === '') {
            $scope.error = 'Password cannot be empty';
            return;
        }
        if (Auth.login($scope.email, $scope.password)) {
            $location.path('/search');
        } else {
            $scope.error = 'Incorrect email or password';
        }
    }
});

whaleApp.controller('SignupCtrl', function ($scope) {

    $scope.email = '';
    $scope.password = '';
    $scope.passwordConfirm = '';

    $scope.validationErrors = [];

    $scope.signedUp = false;

    $scope.signup = function () {
        $scope.validationErrors = [];
        var regex = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
        if (!regex.test($scope.email)) {
            $scope.validationErrors.push('Email is not correct');
            return;
        }
        if ($scope.password === '') {
            $scope.validationErrors.push('Password cannot be empty');
            return;
        }
        if ($scope.password !== $scope.passwordConfirm) {
            $scope.validationErrors.push('Passwords are not equal');
            return;
        }
        localStorage.setItem($scope.email, toMD5($scope.password));
        $scope.signedUp = true;
    }

});

whaleApp.controller('SearchCtrl', function ($scope, Item, Cart) {

    $scope.filterData = {
        //color: 'all'
    };

    Item.list().then(function (data) {
        $scope.items = data;
    });

    $scope.order = function (item) {
        Cart.addItem(item);
        alert('Item has been added to cart');
    };

    $scope.resetFilter = function () {
        $scope.filterData = {
            //color: 'all'
        };
    };

});